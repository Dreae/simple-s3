extern crate reqwest;
extern crate ring;
extern crate http;
extern crate chrono;
extern crate hex;

use http::Uri;
use std::str::FromStr;
use chrono::prelude::*;

pub struct Client {
    access_key: String,
    access_secret: String,
    bucket: String,
    endpoint: String,
    region: String
}

impl Client {
    pub fn new(bucket: String, region: String, access_key: String, access_secret: String) -> Client {
        Client {
            access_key,
            access_secret,
            bucket,
            endpoint: "s3.amazonaws.com".to_owned(),
            region
        }
    }

    pub fn with_endpoint(endpoint: String, bucket: String, region: String, access_key: String, access_secret: String) -> Client {
        Client {
            access_key,
            access_secret,
            bucket,
            endpoint,
            region
        }
    }

    pub fn presigned_url(&self, method: &str, path: &str) -> String {
        let uri = format!("https://{}.{}/{}", self.bucket, self.endpoint, path);
        let parsed = Uri::from_str(&uri).unwrap();

        let date = Utc::now();
        let short_date = date.format("%Y%m%d");
        let full_date = date.format("%Y%m%dT%H%M%SZ");

        let cred = format!("{}%2F{}%2F{}%2Fs3%2Faws4_request", self.access_key, short_date, self.region);
        let query_params = format!("X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential={}&X-Amz-Date={}&X-Amz-Expires=30&X-Amz-SignedHeaders=host",
            cred,
            full_date,
        );
        let headers = format!("host:{}\n", parsed.host().unwrap());
        let canonical_request = format!("{}\n{}\n{}\n{}\n{}\nUNSIGNED-PAYLOAD", method, parsed.path(), query_params, headers, "host");
        let req_hash = ring::digest::digest(&ring::digest::SHA256, &canonical_request.as_bytes());

        let string_to_sign = format!("AWS4-HMAC-SHA256\n{}\n{}/{}/s3/aws4_request\n{}",
            full_date,
            short_date,
            self.region,
            hex::encode(req_hash.as_ref()));

        let mut signing_key = ring::hmac::SigningKey::new(&ring::digest::SHA256, format!("AWS4{}", self.access_secret).as_bytes());
        let date_key = ring::hmac::sign(&signing_key, format!("{}", short_date).as_bytes());
        signing_key = ring::hmac::SigningKey::new(&ring::digest::SHA256, date_key.as_ref());
        let region_key = ring::hmac::sign(&signing_key, self.region.as_bytes());
        signing_key = ring::hmac::SigningKey::new(&ring::digest::SHA256, region_key.as_ref());
        let service_key = ring::hmac::sign(&signing_key, "s3".as_bytes());
        signing_key = ring::hmac::SigningKey::new(&ring::digest::SHA256, service_key.as_ref());
        let req_key = ring::hmac::sign(&signing_key, "aws4_request".as_bytes());
        signing_key = ring::hmac::SigningKey::new(&ring::digest::SHA256, req_key.as_ref());
        let signature = ring::hmac::sign(&signing_key, string_to_sign.as_bytes());

        format!("{}?{}&X-Amz-Signature={}", uri, query_params, hex::encode(signature.as_ref()))
    }
}